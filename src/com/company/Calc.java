package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calc<T extends Calc.CallBack,C extends IController> implements IFactory {
    private T callback;
    private C controller;
    private BufferedReader reader;
    private String operation = null;
    private boolean flagWork = true;

    interface CallBack{
        void callBack(String value);
    }

    public Calc(T callback,C controller) {
        this.callback = callback;
        this.controller=controller;
        reader = new BufferedReader(new InputStreamReader(System.in));
        work();
    }

    private void work(){
        do{
            System.out.println("Выберите оператор: \n"+"| + | - | / | * | Или введите help" );
            try{
                operation = reader.readLine();
            }catch (IOException e) {
                e.printStackTrace();
            }catch (NumberFormatException e){
                System.out.println(Const.MASSAGE_NOT_FORMAT);
            }
            if(operation!=null)help(operation);
            workManager(operation);
        }while(flagWork);
    }

    private void workManager(String operation){
        System.out.println("Введите первое число: ");
        Object numA= calculations();
        System.out.println("Введите второе число: ");
        Object numB = calculations();
        callback.callBack(CalcHelper.getInstance().calculations(numA,operation,numB));
    }

    private void help(String operator) {
        if (operation==null){
            System.out.println(Const.NULL_ERROR);
        }
        if (operation.equals("help")){
            System.out.println("Сделайте выбор \n"+ "1-Вернуться назад,2-Выход");
            int select =(Integer) calculations();
            switch(select){
                case 1:
                    flagWork=false;
                    controller.run();
                    return;
                case 2:
                    System.exit(0);
            }
        }
    }
    private Object calculations(){
        try{
            return Long.parseLong(reader.readLine());
        } catch (IOException e) {
            try {
                return Double.parseDouble(reader.readLine());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
        return Const.NULL_ERROR;
    }
    }

