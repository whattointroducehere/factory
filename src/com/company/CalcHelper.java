package com.company;

public class CalcHelper {
    private static CalcHelper instance = null;

    private CalcHelper() {

    }

    public static synchronized CalcHelper getInstance() {
        if (instance == null) {
            instance = new CalcHelper();
        }
        return instance;
    }

    public <L, R> String calculations(L left, String operator, R right) {
        if (Long.class.isInstance(left) && !Double.class.isInstance(right)) {
            String strA = String.valueOf(left).trim();
            String strB = String.valueOf(right).trim();
            Long numA = Long.parseLong(strA);
            Long numB = Long.parseLong(strB);
            switch (operator) {
                case "+":
                    return String.valueOf(numA + numB);
                case "-":
                    return String.valueOf(numA - numB);
                case "/":
                    if (numB == 0) {
                        System.out.println("На ноль делит нельзя");
                        return "";
                    }
                    if (numA < numB) {
                        return String.valueOf(Double.valueOf(numA) / Double.valueOf(numB));
                    } else {
                        return String.valueOf(numA / numB);
                    }
                case "*":
                    return String.valueOf(numA * numB);
            }

        }
        if (Double.class.isInstance(left) || Double.class.isInstance(right)) {
            String strA = String.valueOf(left);
            String strB = String.valueOf(right);
            Double numA = Double.parseDouble(strA);
            Double numB = Double.parseDouble(strB);
            switch (operator) {
                case "+":
                    return String.valueOf(numA + numB);
                case "-":
                    return String.valueOf(numA - numB);
                case "/":
                    if (numB == 0) {
                        System.out.println("На ноль делит нельзя");
                        return "";
                    }
                    return String.valueOf(numA / numB);
                case "*":
                    return String.valueOf(numA * numB);


            }
        }
        return "";
    }
}
