package com.company;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class Convertor<T extends Convertor.CallBack,C extends IController> implements IFactory {
    private T callback;
    private C controller;
    private String operation = null;
    private boolean flagWork = true;
    private BufferedReader reader;


    interface CallBack{
        void callBack(String value);
    }

    public Convertor(T callback,C controller) {
        this.callback = callback;
        this.controller=controller;
        reader = new BufferedReader(new InputStreamReader(System.in));
        work();
    }

    private void work(){
        do{
            System.out.println("Выберите конвертацию: \n"+"1-Конвертация грн в доллары | 2-Конвертация долларов в грн | Или введите help" );
            try{
                operation = reader.readLine();
            }catch (IOException e) {
                e.printStackTrace();
            }catch (NumberFormatException e){
                System.out.println(Const.MASSAGE_NOT_FORMAT);
            }
            if(operation!=null)help(operation);
            workManager(operation);
        }while(flagWork);
    }

    private void workManager(String operation){
        System.out.println("Введите количество валюты ");
        Object numA= calculations();
        callback.callBack(ConvertHelper.getInstancee().calculationss(numA,operation));
    }
    private void help(String operatorr) {
        if (operation==null){
            System.out.println(Const.NULL_ERROR);
        }
        if (operation.equals("help")){
            System.out.println("Сделайте выбор \n"+ "1-Вернуться назад,2-Выход");
            int select =(Integer) calculations();
            switch(select){
                case 1:
                    flagWork=false;
                    controller.run();
                    return;
                case 2:
                    System.exit(0);
            }
        }
    }
    private Object calculations(){
            try {
                return Double.parseDouble(reader.readLine());
            } catch (IOException e1) {
                e1.printStackTrace();
            }


        return Const.NULL_ERROR;
    }



}








