package com.company;

abstract class Const {
    static final String TAG_CALCK = "calc";
    static final String TAG_CONVERTOR = "convertor";
    static final String ERROR_NUMBER ="Выбрано неверное число, повторите ввод";
    static final String MASSAGE_NOT_FORMAT = "Неверный формат ввода";
    static final String NULL_ERROR = "Ошибка,повторите выбор";
    static final String RESULT="Результат: ";
}
