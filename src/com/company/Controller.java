package com.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Controller implements IController,
        Convertor.CallBack,Calc.CallBack {
    private BufferedReader reader;

    public Controller() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void run() {
        String cmd=null;
        try{
            System.out.println("Выберите пункт: \n"+
                    "1-Калькулятор\n"+
                    "2-Конвертор валют");
            int selectApp=Integer.parseInt(reader.readLine());
            switch(selectApp){
                case 1: cmd=Const.TAG_CALCK;
                break;
                case 2: cmd=Const.TAG_CONVERTOR;
                break;
                default: cmd=Const.ERROR_NUMBER;

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e){
            System.out.println(Const.MASSAGE_NOT_FORMAT);
            run();
        }
        if (cmd==null){
            System.out.println(Const.NULL_ERROR);
            run();
        }
        if (cmd !=null && cmd.equals(Const.ERROR_NUMBER)){
            System.out.println(Const.ERROR_NUMBER);
            run();
        }
        try{
            IFactory factory = Factory.getInstance().factoryMethod(this,this,cmd);
        }catch (Exception e){
            run();
        }
    }

    @Override
    public void callBack(String value) {
        System.out.println(Const.RESULT.concat(" ").concat(value));

    }
}
